%global pypi_name rst.linker
%global pkg_name rst-linker

%bcond_with docs

Summary:        Can add links and perform other custom replacements to rst
Name:           python-%{pkg_name}
Version:        2.4.0
Release:        3%{?dist}
License:        MIT
URL:            https://github.com/jaraco/rst.linker
Source0:        https://files.pythonhosted.org/packages/source/r/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

BuildArch:      noarch

%description
This package provides a routine for adding links and performing other custom replacements to 
restructured text files as a Sphinx extension.

%package -n python3-%{pkg_name}
Summary:        %{summary}

BuildRequires:  python3-devel python3-dateutil
BuildRequires:  python3dist(pathspec) python3dist(path) >= 13
BuildRequires:  python3dist(pip) python3dist(pytest)
BuildRequires:  python3-setuptools_scm >= 1.15.0 python3dist(setuptools)
BuildRequires:  python3dist(toml) python3dist(wheel)
Requires:       python3dist(six) python3-dateutil
Provides:       python3-%{pkg_name} = %{version}-%{release}

%description -n python3-%{pkg_name}
%{description}

%if %{with docs}
%package -n python-%{pkg_name}-doc
Summary:        rst.linker documentation
BuildRequires:  python3dist(sphinx)
BuildRequires:  python3dist(jaraco-packaging)

%description -n python-%{pkg_name}-doc
Documentation for rst.linker
%endif

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%pyproject_wheel
%if %{with docs}
PYTHONPATH=./ sphinx-build docs html

rm -rf html/.{doctrees,buildinfo}
%endif

%install
%pyproject_install

%check
%pytest

%files -n python3-%{pkg_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/rst
%{python3_sitelib}/rst.linker-%{version}.dist-info

%if %{with docs}
%files -n python-%{pkg_name}-doc
%license LICENSE
%doc html
%endif

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.4.0-2
- Rebuilt for loongarch release

* Tue Sep 26 2023 Wang Guodong <gordonwwang@tencent.com> - 2.4.0-1
- Upgrade to version 2.4.0

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.1-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.1-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.1-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.3.1-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Dec 22 2022 Wang Guodong <gordonwwang@tencent.com> - 2.3.1-1
- package init.
